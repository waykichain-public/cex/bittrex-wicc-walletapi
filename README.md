# bittrex-wicc-walletapi

WaykiChain Wallet Restful API for bittrex

Building a wallet for offline signing aka cold wallet implemented in Node.js

<br>

## Getting started

**Build with Dockerfile** 

```bash
cd docker
docker build -t wallet_api . 
```

**Run the image**

```bash
docker run 
```
