const express = require('express');
const router = express.Router();
const db = require('../database/db');
// grab the package.json from SDK directory
const bitcore = require('../../wicc-wallet-utils-js/.');
var WriterHelper = require('../../wicc-wallet-utils-js/lib/util/writerhelper');
// choose the network type.
var arg = {network: 'mainnet'};
var wiccApi = new bitcore.WiccApi(arg);

// @route         GET wallet/transaction
// @description   created a signed transactions using Javascript SDK
// @access        Public

router.post('/', (request, response) => {

    let fromAddr  = request.body.from; // supplied by user
    let toAddr    = request.body.to;
    let amount    = request.body.amount;
    let nonce     = request.body.nonce; //nValidHeight for WICC trx
    let fee       = request.body.fee;
    let gasPrice  = null;
    let gasLimit  = null;

    if (g_locked === false && g_unlock_timeout > 0 && g_unlock_timeout > Date.now()){

        db.get(DBK.LOCKED, function(err, lockedStr) {
            if (err) {
                console.log("error:" + err);
                return err;
            }

            let is_locked = (lockedStr === "true");
            console.log("is_locked: " + is_locked);

            db.get("ADDR_"+fromAddr, function (err, addrNonce) {
                if (err) {
                    console.log("error: " + err);
                    response.json({"errorCode": 1001, "errorMessage": err});
                }

                console.log("fromAddr: " + fromAddr);
                var fromPrivKeyWIF = wiccApi.getPriKeyFromMnemonicCode(g_mnemonic, addrNonce);
                var fromPrivKey = bitcore.PrivateKey.fromWIF(fromPrivKeyWIF);
                var fromPubKey = bitcore.PrivateKey.fromWIF(fromPrivKeyWIF).toPublicKey().toString();

                var destArr = [{
                    "coinType": WriterHelper.prototype.CoinType.WICC,
                    "destAddr": toAddr,
                    "value": amount
                }];

                var cointransferTxinfo = {
                    nTxType: bitcore.WiccApi.UCOIN_TRANSFER_TX,
                    nVersion: 1,
                    nValidHeight: nonce, // should be nonce associated to block height
                    fees: fee,
                    srcRegId: '', // leave as empty string
                    destArr: destArr,
                    publicKey: fromPubKey, // this public key is associated with from address. so get the publickey from from address
                    memo: "",
                    feesCoinType: WriterHelper.prototype.CoinType.WICC,
                    network: 'mainnet'
                };

                var cointransferTx = new bitcore.Transaction.UCoinTransferTx(cointransferTxinfo);
                var hex = cointransferTx.SerializeTx(fromPrivKey);
                response.json({"data": hex});
            });
        });
    }else{
        console.log("the wallet is locked, unlock the wallet first");
        response.status(400).json("the wallet is locked, unlock the wallet first")
    }
});

module.exports = router;
