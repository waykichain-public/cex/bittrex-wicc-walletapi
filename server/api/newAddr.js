const express = require('express');
const router = express.Router();
const db = require('../database/db');
// grab the package.json from SDK directory
const bitcore = require('../../wicc-wallet-utils-js/.');
// aes js to encrypt
var aesjs = require('aes-js');
// choose the network type.
var arg = {network: 'mainnet'};
var wiccApi = new bitcore.WiccApi(arg);

// @route         GET /wallet/address
// @description   locks and unlocks the wallet
// @access        Public

router.post('/', (request, response) => {

    if (g_locked === false && g_unlock_timeout > 0 && g_unlock_timeout > Date.now()){

        db.get(DBK.LASTNONCE, function (err, nonceValue) {
            if (err) {return err}
            // converts to int
            nonceValue = parseInt(nonceValue);
            nonceValue++;

            try {
                var walletInfo = wiccApi.createWalletNonce(g_mnemonic, "", nonceValue);
                // {address -> nonce}

                db.put("ADDR_" + walletInfo.address, nonceValue, function (err) {
                    if (err) { return err }
                    db.put(DBK.LASTNONCE, nonceValue, function (err) {
                        if (err) {return err}
                    })
                });
                console.log(`nonce ${nonceValue}`);
                console.log(walletInfo.address);
                response.json({"address": walletInfo.address});
            }
            catch (err) {
                console.log("Wallet is not unlocked, use Wallet Unlock api first");
                response.json("Wallet is not unlocked, use Wallet Unlock api first")
            }
        })
    }else {
        console.log("Wallet is not unlocked, use Wallet Unlock api first");
        response.json("Wallet is not unlocked, use Wallet Unlock api first")
    }
});

module.exports = router;

