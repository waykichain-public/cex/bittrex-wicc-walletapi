const express = require('express');
const router = express.Router();
const db = require('../database/db');
const utils = require('../utils');
// grab the package.json from SDK directory
const bitcore = require('../../wicc-wallet-utils-js/.');
var arg = {network: 'mainnet'};
var wiccApi = new bitcore.WiccApi(arg);

// @route         put wallet/lock
// @description   locks and unlocks the wallet
// @access        Public

router.get('/status', (req, res) => {
    db.get(DBK.LOCKED, function (err, lockedStr) {
        if (err) {
            console.log("err: " + err);
            return err;
        }

        let is_locked = (lockedStr === "true");

        if (g_unlock_timeout > 0 && g_unlock_timeout < Date.now()) {
            is_locked = true;
        }

        res.json({"locked": is_locked});
    });
});

router.put('/', (request, response) => {

    let toLock = request.body.locked; // true or false = unlock wallet
    let password = request.body.password; // only needed for unlocking the wallet
    let timeout = request.body.timeout; // for unlocking in milliseconds

    if (toLock === true) {
        db.put(DBK.LOCKED, toLock, function (err) {
            if (err) {
                return err
            }
            console.log("wallet is locked");
            response.json("Wallet is locked");
            g_mnemonic = "";
            g_locked = true;
            g_unlock_timeout = 0
        });
    }else {
        // unlocking wallet
        db.put(DBK.LOCKED, toLock, function (err) {
            if (err) {
                return err
            }
            // one time only: first use only when user hasn't used the wallet before
            db.get(DBK.MNEMONIC, function (err, mnemonicValue) {

                //Error no Mnemonic code was create so create one now.
                if (err) {
                    var mnemonic = wiccApi.createAllCoinMnemonicCode(); // only need to create once

                    g_mnemonic = mnemonic; // set newly created mnemonic to global variable;
                    g_locked = false;
                    g_unlock_timeout = Date.now() + timeout;
                    var encryptedMne = utils.AesEncrypt(mnemonic, password); // encrypt the new mnemonic

                    db.put(DBK.MNEMONIC, encryptedMne, function (err) {
                        if (err) {
                            return err
                        }
                        response.json("wallet created and unlocked")
                    })
                }
// ----- If already created a mnemonic code grab the mnemonic code from the db and decrypt it here.
                db.get(DBK.MNEMONIC, function (err, mnemonicValue) {
                    if (err) {return err}
                    // decrypt the value from the db
                    let decryptedMnemonic = utils.AesDecrypt(mnemonicValue, password);

                    var engLetters = /^[A-Za-z]+/; // checks if mnemonic was decrypted correctly
                    if(decryptedMnemonic.match(engLetters)) {
                        response.status(200).json("Wallet unlocked");
                        console.log("password is correct");
                    }
                    else
                    {
                        console.log("password is wrong");
                        response.status(403).json("incorrect password entered for this wallet")
                    }
                    g_mnemonic = decryptedMnemonic;
                    g_locked = false;
                    g_unlock_timeout = Date.now() + timeout; //where time gets set
                })
            })
        });
    }
});

module.exports = router;
